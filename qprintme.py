from PyQt6 import uic
from PyQt6.QtWidgets import QApplication, QTextEdit
from PyQt6.QtPrintSupport import QPrinter, QPrintPreviewDialog

class Main:
    def __init__(self):
        self.printformui = uic.loadUi('uiprint.ui')
        self.printformui.show()
        self.printformui.pbPrint.clicked.connect(self.toprint)

    def toprint(self):
        fn = self.printformui.lineEditfn.text()
        ln = self.printformui.lineEditln.text()
        
        self.mytext = QTextEdit() 
        
        self.mytext.setPlainText("First Name: " + fn + "\n"
                                                 "Last Name: " + ln + "\n")
        
        printer = QPrinter()
        previewDialog = QPrintPreviewDialog(printer)
        previewDialog.paintRequested.connect(self.mytext.print)

        previewDialog.exec()

if __name__ == '__main__':
    app = QApplication([])
    main = Main()
    app.exec()

"""

# This is PyQt6 with QTextDocument to manipulate design for data report before to print

from PyQt6 import uic
from PyQt6.QtWidgets import QApplication
from PyQt6.QtGui import QTextDocument
from PyQt6.QtPrintSupport import QPrinter, QPrintPreviewDialog

class Main:
    def __init__(self):
        self.printformui = uic.loadUi('uiprint.ui')
        self.printformui.show()
        self.printformui.pbPrint.clicked.connect(self.toprint)

    def toprint(self):
        fn = self.printformui.lineEditfn.text()
        ln = self.printformui.lineEditln.text()

        html_text = f"<font size='12'>First Name:</font> {fn}<br><font size='12'>Last Name:</font> {ln}"

        document = QTextDocument()
        document.setHtml(html_text)
        
        printer = QPrinter()
        previewDialog = QPrintPreviewDialog(printer)
        previewDialog.paintRequested.connect(lambda: document.print(printer))

        previewDialog.exec()

if __name__ == '__main__':
    app = QApplication([])
    main = Main()
    app.exec()

----------------------------------------------------------------------------------------------------

# pyqt6 library simple print with QTextEdit

from PyQt6 import uic
from PyQt6.QtWidgets import QApplication
from PyQt6.QtGui import QTextDocument
from PyQt6.QtPrintSupport import QPrinter, QPrintPreviewDialog

class Main:
    def __init__(self):
        self.printformui = uic.loadUi('uiprint.ui')
        self.printformui.show()
        self.printformui.pbPrint.clicked.connect(self.toprint)

    def toprint(self):
        fn = self.printformui.lineEditfn.text()
        ln = self.printformui.lineEditln.text()

        self.printformui.txtPreview.setPlainText("First Name: " + fn + "\n"
                                                 "Last Name: " + ln + "\n")

        #html_text = f"<font size='12'>First Name:</font> {fn}<br><font size='12'>Last Name:</font> {ln}"

        #document = QTextDocument()
        #document.setHtml(html_text)
        
        printer = QPrinter()
        previewDialog = QPrintPreviewDialog(printer)
        previewDialog.paintRequested.connect(self.printformui.txtPreview.print)#lambda: document.print(printer))

        previewDialog.exec()

if __name__ == '__main__':
    app = QApplication([])
    main = Main()
    app.exec()

-----------------------------------------------------------------------------------------------

# with pyqt5 library

from PyQt5 import uic
from PyQt5.QtWidgets import *
from PyQt5.QtPrintSupport import QPrinter, QPrintPreviewDialog
#from PyQt5.QtGui import QTextDocument

class Main:
    def __init__(self):
        self.printformui = uic.loadUi('uiprint.ui')
        self.printformui.show()
        self.printformui.pbPrint.clicked.connect(self.toprint)

    def toprint(self):
        fn = self.printformui.lineEditfn.text()
        ln = self.printformui.lineEditln.text()

        self.printformui.txtPreview.setPlainText("First Name: " + fn + "\n"
                                                 "Last Name: " + ln + "\n")
    
        printer = QPrinter(QPrinter.HighResolution)
        previewDialog = QPrintPreviewDialog(printer)
        previewDialog.paintRequested.connect(self.printformui.txtPreview.print_)
    
        previewDialog.exec_()

if __name__ == '__main__':
    app = QApplication([])
    main = Main()
    app.exec()

"""